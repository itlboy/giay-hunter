<?php
// HTTP
define('HTTP_SERVER', 'http://giay-hunter.dev.mobileplus.vn/admin/');
define('HTTP_CATALOG', 'http://giay-hunter.dev.mobileplus.vn/');
define('HTTP_IMAGE', 'http://giay-hunter.dev.mobileplus.vn/image/');

// HTTPS
define('HTTPS_SERVER', 'http://giay-hunter.dev.mobileplus.vn/admin/');
define('HTTPS_CATALOG', 'http://giay-hunter.dev.mobileplus.vn/');
define('HTTPS_IMAGE', 'http://giay-hunter.dev.mobileplus.vn/image/');

// DIR
define('DIR_APPLICATION', '/home/giay-hunter.dev.mobileplus.vn/public_html/admin/');
define('DIR_SYSTEM', '/home/giay-hunter.dev.mobileplus.vn/public_html/system/');
define('DIR_DATABASE', '/home/giay-hunter.dev.mobileplus.vn/public_html/system/database/');
define('DIR_LANGUAGE', '/home/giay-hunter.dev.mobileplus.vn/public_html/admin/language/');
define('DIR_TEMPLATE', '/home/giay-hunter.dev.mobileplus.vn/public_html/admin/view/template/');
define('DIR_CONFIG', '/home/giay-hunter.dev.mobileplus.vn/public_html/system/config/');
define('DIR_IMAGE', '/home/giay-hunter.dev.mobileplus.vn/public_html/image/');
define('DIR_CACHE', '/home/giay-hunter.dev.mobileplus.vn/public_html/system/cache/');
define('DIR_DOWNLOAD', '/home/giay-hunter.dev.mobileplus.vn/public_html/download/');
define('DIR_LOGS', '/home/giay-hunter.dev.mobileplus.vn/public_html/system/logs/');
define('DIR_CATALOG', '/home/giay-hunter.dev.mobileplus.vn/public_html/catalog/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '14121517');
define('DB_DATABASE', 'giay-hunter');
define('DB_PREFIX', 'oc_');
?>