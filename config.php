<?php
// HTTP
$server = $_SERVER['HTTP_HOST'];
define('HTTP_SERVER', "http://$server/");
define('HTTP_IMAGE', 'http://'.$server.'/image/');
define('HTTP_ADMIN', 'http://'.$server.'/admin/');

// HTTPS
define('HTTPS_SERVER', 'http://'.$server.'/');
define('HTTPS_IMAGE', 'http://'.$server.'/image/');

$currentDir = __DIR__;
// DIR
define('DIR_APPLICATION', $currentDir.'/catalog/');
define('DIR_SYSTEM', $currentDir.'/system/');
define('DIR_DATABASE', $currentDir.'/system/database/');
define('DIR_LANGUAGE', $currentDir.'/language/');
define('DIR_TEMPLATE', $currentDir.'/catalog/view/theme/');
define('DIR_CONFIG', $currentDir.'/system/config/');
define('DIR_IMAGE', $currentDir.'/image/');
define('DIR_CACHE', '/home/giay-hunter.dev.mobileplus.vn/public_html/system/cache/');
define('DIR_DOWNLOAD', '/home/giay-hunter.dev.mobileplus.vn/public_html/download/');
define('DIR_LOGS', '/home/giay-hunter.dev.mobileplus.vn/public_html/system/logs/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '14121517');
define('DB_DATABASE', 'giay-hunter');
define('DB_PREFIX', 'oc_');
?>